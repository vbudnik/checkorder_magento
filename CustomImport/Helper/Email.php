<?php

namespace Hunters\CustomImport\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;
use Hunters\CustomImport\Helper\Data;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;
    protected $storeConfig;
    protected $configData;


    public function __construct(
        Context $context,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        Data $configData
    ) {
        parent::__construct($context);
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->logger = $context->getLogger();
        $this->configData = $configData;
    }

    public function getSenderData()
    {
        $senderEmail = $this->configData->getGeneralConfig('sender_email');
        $sender = [
            'email' => $senderEmail,
        ];
        return $sender;
    }

    public function getReciverData()
    {
        $reciverEmail = $this->configData->getGeneralConfig('receiver_email');

        $reciver = [
            'name' => "Admin",
            'email' => $reciverEmail,
        ];

        return $reciver;
    }

    public function sendEmail($fileName)
    {

        try {
            $sender = $this->getSenderData();
            $reciverEmail = $this->getReciverData();

            $this->inlineTranslation->suspend();
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('email_demo_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars([
                    'templateVar'  => 'My Topic',
                ])
                ->setFrom($reciverEmail)
                ->addTo($sender)
                ->addAttachment($fileName, $fileName)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            return true;
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Can't send e-mail."));
            $this->logger->debug($e->getMessage());
        }
    }
}