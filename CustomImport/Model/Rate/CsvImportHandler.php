<?php

namespace Hunters\CustomImport\Model\Rate;

use Magento\Framework\App\Filesystem\DirectoryList;

class CsvImportHandler
{
    protected $_publicStores;
    protected $csvProcessor;
    protected $directory;
    protected $fileFactory;
    protected $email;



    public function __construct(
        \Magento\Store\Model\ResourceModel\Store\Collection $storeCollection,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Hunters\CustomImport\Helper\Email $email
    ) {
        $this->_publicStores = $storeCollection->setLoadDefault(false);
        $this->csvProcessor = $csvProcessor;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->fileFactory = $fileFactory;
        $this->email = $email;

    }

    public function getColumnHeader()
    {
        return [
            'ORDER_NO',
            'SOURCE_ID',
            'DATE_LOADED',
            'DATE_ORDERED',
            'CL_NO',
            'F_NAME',
            'L_NAME',
        ];
    }

    public function importFromCsvFile($file)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
        $ratesRawData = $this->csvProcessor->getData($file['tmp_name']);
        $fileFields = $ratesRawData[0];
        $validFields = $this->getColumnHeader($fileFields);
        $invalidFields = array_diff($fileFields, $validFields);
        $ratesData = $this->_filterRateData($ratesRawData, $invalidFields, $validFields);
        $notExistOrder = [];
        foreach ($ratesData as $rowIndex => $dataRow) {
            if ($rowIndex == 0) {
                continue;
            }
            if ($this->checkOrderQueue($dataRow) == false && $this->checkOrderCartRower($dataRow) == false) {
                $notExistOrder[] = $dataRow;
                $this->setNotExistData($dataRow);
            }
        }
        if (!empty($notExistOrder)) {
            $filePath = $this->createCsv($notExistOrder);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
            $rootPath = $directory->getRoot();
            $this->email->sendEmail($rootPath."/var/".$filePath);
        }
    }

    protected function checkInsertArray(array $dataRow)
    {
        if (count($dataRow) != count($this->getColumnHeader())) {
            throw new \Magento\Framework\Exception\LocalizedException(__("Invalid data in file. Not empty line in file"));
        }
        foreach ($dataRow as $row) {
            if (empty($row)) {
                throw new \Magento\Framework\Exception\LocalizedException(__("Invalid data in file. Not empty line in file"));
            }
        }
    }

    protected function setNotExistData(array $dataRow)
    {
        $this->checkInsertArray($dataRow);
        $insertRow = intval($dataRow[0]) .", ". intval($dataRow[1]) .", ". intval($dataRow[2]) .", ". $dataRow[3] .
            ", '". $dataRow[4] ."', '". $dataRow[5] ."', '". $dataRow[6] ."'";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableCartRoverOrders = $resource->getTableName('cart_rover_orders');
        $sql = "insert into ".$tableCartRoverOrders." (order_no, source_id, data_loaded, data_ordered, cl_no, f_name, l_name ) value ( ".$insertRow." )";
        $result = $connection->query($sql);
        return $result;
    }

    protected function _filterFileFields(array $fileFields)
    {
        $filteredFields = $this->getRequiredCsvFields();
        $requiredFieldsNum = count($this->getRequiredCsvFields());
        $fileFieldsNum = count($fileFields);

        for ($index = $requiredFieldsNum; $index < $fileFieldsNum; $index++) {
            $titleFieldName = $fileFields[$index];
            $filteredFields[$index] = $titleFieldName;
        }

        return $filteredFields;
    }


    protected function _filterRateData(array $rateRawData, array $invalidFields, array $validFields)
    {
        $validFieldsNum = count($validFields);
        foreach ($rateRawData as $rowIndex => $dataRow) {
            if (count($dataRow) <= 1) {
                unset($rateRawData[$rowIndex]);
                continue;
            }
            foreach ($dataRow as $fieldIndex => $fieldValue) {
                if (isset($invalidFields[$fieldIndex])) {
                    unset($rateRawData[$rowIndex][$fieldIndex]);
                }
            }
            if (count($rateRawData[$rowIndex]) != $validFieldsNum) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file format.'));
            }
        }
        return $rateRawData;
    }

    protected function checkOrderCartRower(array $rateData)
    {
        $orderId = $rateData[0];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableCartRover = $resource->getTableName('cart_rover_orders');
        $sql = "select order_no from ".$tableCartRover." where order_no = " .$orderId . " limit 1";
        $result = $connection->fetchAll($sql);
        if ($result == NULL)
            return false;
        return true;
    }

    protected function checkOrderQueue(array $rateData)
    {
        $orderId = $rateData[0];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableSapOrderQueue = $resource->getTableName('sap_order_queue');
        $sql = "select order_id from ". $tableSapOrderQueue ." where order_id = " . $orderId . " limit 1";
        $result = $connection->fetchAll($sql);
        if ($result == NULL)
            return false;
        return true;
    }

    protected function createCsv(array $dataRow)
    {
        $name = date('m_d_Y_H_i-s');
        $filePath = 'export/custom' . $name . '.csv';
        $this->directory->create('export');
        $stream = $this->directory->openFile($filePath, 'w+');
        $stream->lock();
        $columns = $this->getColumnHeader();
        foreach ( $columns as $column) {
            $header[] = $column;
        }
        $stream->writeCsv($header);
        foreach ($dataRow as $item) {
            $stream->writeCsv($item);
        }
        $stream->close();
        return $filePath;
    }

}

