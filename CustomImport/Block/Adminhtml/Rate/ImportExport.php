<?php

namespace Hunters\CustomImport\Block\Adminhtml\Rate;

class ImportExport extends \Magento\Backend\Block\Widget
{

    protected $_template = 'Hunters_CustomImport::importExport.phtml';


    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->setUseContainer(true);
    }
}
