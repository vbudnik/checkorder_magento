<?php

namespace Hunters\CheckOrder\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CheckOrder implements ObserverInterface
{
    const is_downloadable = "downloadable";

    protected $logger;


    /**
     * Data constructor.
     *
     * @param \Psr\Log\LoggerInterface                                 $logger Logger
     * @param array                                                    $data Array with data
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger, array $data = [])
    {
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        try {
            //get information about order
            $order = $observer->getEvent()->getOrder();
            $orderId = $order->getIncrementId();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableSalesOrder = $resource->getTableName('sales_order');
            $tableSalesOrderItem = $resource->getTableName('sales_order_item');
            $sql = 'select product_type from ' . $tableSalesOrderItem . ' where order_id = '.
                ' (select entity_id from ' . $tableSalesOrder . ' order by entity_id DESC limit 1)';
            $productType = $connection->fetchAll($sql);
            $countAllProduct = count($productType);
            $countDownloadableItem = 0;
            foreach ($productType as $product) {
                if ($product['product_type'] == self::is_downloadable)
                    $countDownloadableItem++;
            }
            // if all product in order in's downloadable set flag 1
            $value = ($countAllProduct == $countDownloadableItem ? 1 : 0);
            try {
                $sql = 'UPDATE '. $tableSalesOrder .' SET is_downloadable = '. $value .' where increment_id = '. $orderId;
                $resulst = $connection->fetchAll($sql);
            } catch (\Exception $exception) {
                $this->logger->debug($exception->getMessage());
            }
        } catch (\Exception $exception) {
            $this->logger->debug($exception->getMessage());
        }
    }
}

/*
 *  For update databases
 *
# select  oi.increment_id from sales_order_item as soi join sales_order as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT  o.increment_id FROM sales_order_item AS oi INNER JOIN sales_order AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type);
# select o.increment_id, soi.name, soi.product_type, o.is_downloadable from sales_order_item as soi join sales_order o on soi.order_id = o.entity_id and o.increment_id in(select oi.increment_id from sales_order_item as soi join sales_order as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT distinct o.increment_id FROM sales_order_item AS oi INNER JOIN sales_order AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type));

# For update value of 1
# update sales_order set is_downloadable = 1 where increment_id in (select distinct oi.increment_id from sales_order_item as soi join (select * from sales_order) as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT  o.increment_id FROM sales_order_item AS oi INNER JOIN (select * from sales_order) AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type));
# select distinct o.increment_id, soi.name, soi.product_type, o.is_downloadable from sales_order_item as soi join sales_order o on soi.order_id = o.entity_id and o.increment_id in(select oi.increment_id from sales_order_item as soi join sales_order as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT distinct o.increment_id FROM sales_order_item AS oi INNER JOIN sales_order AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type));

# For update value of 0
# update sales_order set is_downloadable = 0 where increment_id not in (select distinct oi.increment_id from sales_order_item as soi join (select * from sales_order) as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT  o.increment_id FROM sales_order_item AS oi INNER JOIN (select * from sales_order) AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type));
# select increment_id, is_downloadable from sales_order where increment_id not in (select distinct oi.increment_id from sales_order_item as soi join (select * from sales_order) as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT  o.increment_id FROM sales_order_item AS oi INNER JOIN (select * from sales_order) AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type));

# select so.increment_id, so.is_downloadable, soi.name, soi.product_type from sales_order as so join sales_order_item as soi on so.entity_id = soi.order_id and soi.product_type = 'downloadable';
# select distinct so.increment_id from sales_order as so join sales_order_item as soi on so.entity_id = soi.order_id and soi.product_type = 'downloadable';

# select so.increment_id, so.is_downloadable, soi.name, soi.product_type from sales_order as so join sales_order_item as soi on so.entity_id = soi.order_id and so.increment_id not in ((select distinct oi.increment_id from sales_order_item as soi join (select * from sales_order) as oi on oi.entity_id = soi.order_id and oi.increment_id in (SELECT  o.increment_id FROM sales_order_item AS oi INNER JOIN (select * from sales_order) AS o ON o.entity_id = oi.order_id WHERE 1 and oi.product_type = 'downloadable') group by oi.increment_id having count(case  when soi.product_type = 'downloadable' then 1 else null end) = count(soi.product_type)));
 * */
