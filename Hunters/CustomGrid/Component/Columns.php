<?php

namespace Hunters\CustomGrid\Component;

use Magento\Customer\Api\Data\AttributeMetadataInterface as AttributeMetadata;

class Columns extends \Magento\Customer\Ui\Component\Listing\Columns
{
    private $remoteColumn = ['dob', 'taxvat', 'gender'];

    public function prepare()
    {
        $this->columnSortOrder = $this->getDefaultSortOrder();
        foreach ($this->attributeRepository->getList() as $newAttributeCode => $attributeData) {
            if (in_array($newAttributeCode, $this->remoteColumn)) {
                continue ;
            }
            if (isset($this->components[$newAttributeCode])) {
                $this->updateColumn($attributeData, $newAttributeCode);
            } elseif (!$attributeData[AttributeMetadata::BACKEND_TYPE] != 'static'
                && $attributeData[AttributeMetadata::IS_USED_IN_GRID]
            ) {
                $this->addColumn($attributeData, $newAttributeCode);
            }
        }
        $this->updateActionColumnSortOrder();
        \Magento\Ui\Component\Listing\Columns::prepare();
    }
}