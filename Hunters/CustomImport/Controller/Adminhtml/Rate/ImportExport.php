<?php

namespace Hunters\CustomImport\Controller\Adminhtml\Rate;

use Magento\Framework\Controller\ResultFactory;

class ImportExport extends \Hunters\CustomImport\Controller\Adminhtml\Rate
{

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('Hunters_CustomImport::system_customimport');
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock(\Hunters\CustomImport\Block\Adminhtml\Rate\ImportExport::class)
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Custom import file order'));
        $resultPage->getConfig()->getTitle()->prepend(__('Custom import file order'));
        return $resultPage;
    }
}
