<?php

namespace Hunters\CustomImport\Controller\Adminhtml\Rate;

use Magento\Framework\Controller\ResultFactory;

class ImportPost extends \Hunters\CustomImport\Controller\Adminhtml\Rate
{

    public function execute()
    {
        if ($this->getRequest()->isPost() && !empty($_FILES['import_rates_file']['tmp_name'])) {
            try {
                $importHandler = $this->_objectManager->create(
                    \Hunters\CustomImport\Model\Rate\CsvImportHandler::class
                );
                $importHandler->importFromCsvFile($this->getRequest()->getFiles('import_rates_file'));

                $this->messageManager->addSuccess(__('The order list has been imported.'));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Invalid file upload attempt'));
            }
        } else {
            $this->messageManager->addError(__('Invalid file upload attempt'));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRedirectUrl());
        return $resultRedirect;
    }

}
