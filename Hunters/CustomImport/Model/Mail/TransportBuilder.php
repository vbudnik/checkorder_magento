<?php

namespace Hunters\CustomImport\Model\Mail;

use Magento\Framework\Mail\MessageInterfaceFactory;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{

    public function addAttachment($path, $name)
    {
        if (!empty($path) && file_exists($path)) {
            if ($name == NULL) {
                $name = 'NotExistOrderCartRover';
            }
            $this->message->createAttachment(
                file_get_contents($path),
                \Zend_Mime::TYPE_OCTETSTREAM,
                \Zend_Mime::DISPOSITION_ATTACHMENT,
                \Zend_Mime::ENCODING_BASE64,
                basename($name)
            );
        }
        return $this;
    }
}